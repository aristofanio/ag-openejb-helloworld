# EJB Example

An review about example shown in OpenEJB site.

## How do

Execute EJB plataform (lite):

```
$ ./openejb/bin/openejb start
Apache OpenEJB 3.1.1    build: 20090530-06:18
http://openejb.apache.org/
[init] OpenEJB Remote Server
  ** Starting Services **
  NAME                 IP              PORT  
  httpejbd             127.0.0.1       4204  
  telnet               127.0.0.1       4202  
  hsql                 127.0.0.1       9001  
  ejbd                 127.0.0.1       4201  
  admin thread         127.0.0.1       4200  
-------
Ready!
```

Compile your ejb-server:
```
$ sh compile.sh
added manifest
adding: org/(in = 0) (out= 0)(stored 0%)
adding: org/acme/(in = 0) (out= 0)(stored 0%)
adding: org/acme/HelloBean.class(in = 753) (out= 394)(deflated 47%)
adding: org/acme/HelloHome.java(in = 196) (out= 136)(deflated 30%)
adding: org/acme/HelloObject.java(in = 180) (out= 130)(deflated 27%)
adding: org/acme/HelloWorld.java(in = 1552) (out= 731)(deflated 52%)
adding: org/acme/HelloObject.class(in = 234) (out= 178)(deflated 23%)
adding: org/acme/HelloBean.java(in = 503) (out= 220)(deflated 56%)
adding: org/acme/HelloHome.class(in = 263) (out= 189)(deflated 28%)
ignoring entry META-INF/
adding: META-INF/ejb-jar.xml(in = 651) (out= 272)(deflated 58%)
```
So deploy it:
```
$ sh deploy.sh 
Module with moduleId "Hello" does not exist.
Successfully undeployed module with moduleId "/Users/ariaraujo/Downloads/ejb-test/openejb/apps/myHelloEjb.jar" .
Application deployed successfully at "myHelloEjb.jar"
App(id=/Users/ariaraujo/Downloads/ejb-test/openejb/apps/myHelloEjb.jar)
    EjbJar(id=myHelloEjb.jar, path=/Users/ariaraujo/Downloads/ejb-test/openejb/apps/myHelloEjb.jar)
        Ejb(ejb-name=Hello, id=Hello)
            Jndi(name=HelloRemoteHome)
```

Now compile and execute ejb-client:
```
$ sh run-client.sh 
classpath=.:openejb/lib/XmlSchema-1.4.2.jar:openejb/lib/activeio-core-3.0.0-incubator.jar:openejb/lib/activemq-core-4.1.1.jar:openejb/lib/activemq-ra-4.1.1.jar:openejb/lib/backport-util-concurrent-2.1.jar:openejb/lib/bcprov-jdk15-140.jar:openejb/lib/commons-cli-1.1.jar:openejb/lib/commons-collections-3.2.jar:openejb/lib/commons-dbcp-all-1.3-r699049.jar:openejb/lib/commons-lang-2.1.jar:openejb/lib/commons-logging-1.1.jar:openejb/lib/commons-pool-1.3.jar:openejb/lib/cxf-bundle-2.0.9.jar:openejb/lib/ejb31-api-experimental-3.1.1.jar:openejb/lib/geronimo-connector-2.1.jar:openejb/lib/geronimo-javamail_1.4_mail-1.2.jar:openejb/lib/geronimo-transaction-2.1.jar:openejb/lib/howl-1.0.1-1.jar:openejb/lib/hsqldb-1.8.0.7.jar:openejb/lib/javaee-api-5.0-2.jar:openejb/lib/jaxb-impl-2.0.5.jar:openejb/lib/log4j-1.2.12.jar:openejb/lib/neethi-2.0.4.jar:openejb/lib/openejb-api-3.1.1.jar:openejb/lib/openejb-client-3.1.1.jar:openejb/lib/openejb-core-3.1.1.jar:openejb/lib/openejb-cxf-3.1.1.jar:openejb/lib/openejb-ejbd-3.1.1.jar:openejb/lib/openejb-hsql-3.1.1.jar:openejb/lib/openejb-http-3.1.1.jar:openejb/lib/openejb-javaagent-3.1.1.jar:openejb/lib/openejb-jee-3.1.1.jar:openejb/lib/openejb-loader-3.1.1.jar:openejb/lib/openejb-multicast-3.1.1.jar:openejb/lib/openejb-server-3.1.1.jar:openejb/lib/openejb-telnet-3.1.1.jar:openejb/lib/openejb-webservices-3.1.1.jar:openejb/lib/openjpa-1.2.0.jar:openejb/lib/opensaml-1.1.jar:openejb/lib/quartz-1.5.2.jar:openejb/lib/saaj-impl-1.3.jar:openejb/lib/serp-1.13.1.jar:openejb/lib/slf4j-api-1.3.1.jar:openejb/lib/slf4j-jdk14-1.3.1.jar:openejb/lib/stax-api-1.0.1.jar:openejb/lib/swizzle-stream-1.0.1.jar:openejb/lib/wsdl4j-1.6.1.jar:openejb/lib/wss4j-1.5.4.jar:openejb/lib/wstx-asl-3.2.0.jar:openejb/lib/xbean-asm-shaded-3.6-r779512.jar:openejb/lib/xbean-finder-shaded-3.6-r779512.jar:openejb/lib/xbean-naming-3.5.jar:openejb/lib/xbean-reflect-3.6-r779512.jar:openejb/lib/xml-resolver-1.2.jar:openejb/lib/xmlsec-1.4.0.jar
Hello World!!!!!
```

## Screenshots

#### Server
![https://gitlab.com/aristofanio/ag-openejb-helloworld/raw/master/screenshot/Screen%20Shot%202019-03-03%20at%2010.28.12.png](https://gitlab.com/aristofanio/ag-openejb-helloworld/raw/master/screenshot/Screen%20Shot%202019-03-03%20at%2010.28.12.png)


#### Compiling
![https://gitlab.com/aristofanio/ag-openejb-helloworld/raw/master/screenshot/Screen%20Shot%202019-03-03%20at%2009.33.52.png](https://gitlab.com/aristofanio/ag-openejb-helloworld/raw/master/screenshot/Screen%20Shot%202019-03-03%20at%2009.33.52.png)

#### Deployment
![https://gitlab.com/aristofanio/ag-openejb-helloworld/raw/master/screenshot/Screen%20Shot%202019-03-03%20at%2009.32.31.png](https://gitlab.com/aristofanio/ag-openejb-helloworld/raw/master/screenshot/Screen%20Shot%202019-03-03%20at%2009.32.31.png)

#### HelloWorld
![https://gitlab.com/aristofanio/ag-openejb-helloworld/raw/master/screenshot/Screen%20Shot%202019-03-03%20at%2009.41.50.png](https://gitlab.com/aristofanio/ag-openejb-helloworld/raw/master/screenshot/Screen%20Shot%202019-03-03%20at%2009.41.50.png)


## References
 - http://svn.apache.org/repos/asf/tomee/openejb/branches/openejb0/src/server/webadmin/hello-world.html. 
