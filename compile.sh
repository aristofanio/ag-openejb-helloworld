#!/bin/bash

#local
OPENEJB_HOME=$(pwd)/openejb
PATH=$PATH:$OPENEJB_HOME/bin:$JAVA_HOME/bin

#classpath
CLASSPATH="."
for entry in `ls openejb/lib`; do
    CLASSPATH=$CLASSPATH:openejb/lib/$entry
done

#remove olds
rm org/acme/*.class
rm myHelloEjb.jar

#compile: server
javac -cp $CLASSPATH:. org/acme/HelloObject.java org/acme/HelloHome.java org/acme/HelloBean.java
jar cvf myHelloEjb.jar org META-INF


