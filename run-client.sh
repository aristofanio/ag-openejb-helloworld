#!/bin/bash

#local
OPENEJB_HOME=$(pwd)/openejb
PATH=$PATH:$OPENEJB_HOME/bin:$JAVA_HOME/bin

#classpath
CLASSPATH="."
for entry in `ls openejb/lib`; do
    CLASSPATH=$CLASSPATH:openejb/lib/$entry
done
echo "classpath=$CLASSPATH"

#compile: client
javac -cp $CLASSPATH:. org/acme/HelloWorld.java

#run
java -cp $CLASSPATH:. -Dopenejb.home=$OPENEJB_HOME org.acme.HelloWorld

